#!/bin/sh
# DESCRIPTION
#
#   OSX version of useradd
#
# LICENSE
#
#   Copyright (c) 2014 Andy May <ajmay81@gmail.com>
#
#   Copying and distribution of this file, with or without modification, are
#   permitted in any medium without royalty provided the copyright notice
#   and this notice are preserved. This file is offered as-is, without any
#   warranty.

comment=""
createhome=""
login=""
basedir="/Users"
home=""
shell=""
system=""
gid=""
uid=""
password=""

usage () {
 cat << _EOF_
Usage: useradd [options] LOGIN

Options:
  -c, --comment COMMENT         GECOS field of the new account
  -d, --home-dir HOME_DIR       home directory of the new account
  -g, --gid GROUP               name or ID of the primary group of the new
                                account
  -h, --help                    display this help message and exit
  -m, --create-home             create the user's home directory
  -p, --password PASSWORD       password of the new account
  -r, --system                  create a system account
  -s, --shell SHELL             login shell of the new account
  -u, --uid UID                 user ID of the new account
_EOF_
 exit ${1}
}

while test "x${1}" != x ; do
 case "x${1}" in
  x-c                     ) shift; comment="${1}" ;;
  x-d                     ) shift; home="${1}" ;;
  x-g                     ) shift; gid="${1}" ;;
  x-h | x--help           ) usage 0 ;;
  x-m | x--create-home    ) createhome="true" ;;
  x-M | x--no-create-home ) createhome="" ;;
  x-p                     ) shift; password="${1}" ;;
  x-r | x--system         ) system="true" ;;
  x-s                     ) shift; shell="${1}" ;;
  x-u                     ) shift; uid="${1}" ;;
  x-*                     ) echo "${0}: invalid option '${1}'"; usage 1 ;;
  x*                      ) if test "x${login}" = x ; then
                             login="${1}"
                            else
                             echo "LOGIN already given"; exit 1
                            fi ;;
 esac
 shift
done

if test "x${login}" = x ; then
 echo "no LOGIN given"; exit 1
fi
dscl . create ${basedir}/${login}

if test "x${shell}" = x ; then
 shell="/bin/bash"
fi
dscl . create ${basedir}/${login} UserShell "${shell}"

if test "x${comment}" != x ; then
 dscl . create ${basedir}/${login} RealName "${comment}"
fi

if test "x${uid}" = x ; then
 highest_uid="`dscl . -list /Users UniqueID | sort -nr -k 2 | head -1 | grep -oE '[0-9]+$'`"
 uid="`expr ${highest_uid} + 1`"
fi
dscl . create ${basedir}/${login} UniqueID "${uid}"

if test "x${gid}" = x ; then
#gid="1000"
 gid="20"
fi
dscl . create ${basedir}/${login} PrimaryGroupID "${gid}"

if test "x${home}" = x ; then
 home="${basedir}/${login}"
fi
dscl . create ${basedir}/${login} NFSHomeDirectory "${home}"
if test "x${createhome}" != x ; then
 if test ! -r "${home}" ; then
  createhomedir -c -u ${login}
 fi
fi

if test "x${password}" != x ; then
 dscl . passwd ${basedir}/${login} "${password}"
fi

if test "x${system}" != x ; then
 dscl . append /Groups/admin GroupMembership ${login}
fi
