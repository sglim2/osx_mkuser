#!/bin/sh
# DESCRIPTION
#
#   OSX version of userdel
#
# LICENSE
#
#   Copyright (c) 2014 Andy May <ajmay81@gmail.com>
#
#   Copying and distribution of this file, with or without modification, are
#   permitted in any medium without royalty provided the copyright notice
#   and this notice are preserved. This file is offered as-is, without any
#   warranty.

basedir="/Users"
comment=""
login=""
remove=""

usage () {
 cat << _EOF_
Usage: userdel [options] LOGIN

Options:
  -h, --help                    display this help message and exit
  -r, --remove                  remove home directory
_EOF_
 exit ${1}
}

while test "x${1}" != x ; do
 case "x${1}" in
  x-h | x--help   ) usage 0 ;;
  x-r | x--remove ) remove="true" ;;
  x-*             ) echo "${0}: invalid option '${1}'"; usage 1 ;;
  x*              ) if test "x${login}" = x ; then
                     login="${1}"
                    else
                     echo "LOGIN already given"; exit 1
                    fi ;;
 esac
 shift
done

if test "x${login}" = x ; then
 echo "no LOGIN given"; exit 1
fi

if test "x${remove}" != x ; then
 home="`dscl . -read ${basedir}/${login} NFSHomeDirectory | sed -e 's/^[^ ]*  *//'`"
 rm -rf ${home}
fi

dscl . delete ${basedir}/${login}
